package by.rozmysl;

import net.minidev.json.JSONObject;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;

public class JSONEventLayoutV1 extends Layout {

    private final boolean ignoreThrowable = false;
    public static final FastDateFormat TIME = FastDateFormat.getInstance("yyyy-MM-dd' 'HH:mm:ss.SSS");

    public JSONEventLayoutV1() {
        this(true);
    }

    public JSONEventLayoutV1(boolean locationInfo) {
    }

    public String format(LoggingEvent loggingEvent) {
        long time = loggingEvent.getTimeStamp();
        JSONObject logstashEvent = new JSONObject();
        logstashEvent.put("@time", TIME.format(time));
        logstashEvent.put("message", loggingEvent.getRenderedMessage());
        logstashEvent.put("level", loggingEvent.getLevel().toString());
        logstashEvent.put("file", loggingEvent.getLocationInformation().getFileName());
        return logstashEvent.toString() + "\n";
    }

    public boolean ignoresThrowable() {
        return ignoreThrowable;
    }

    public void activateOptions() {
        boolean activeIgnoreThrowable = ignoreThrowable;
    }
}