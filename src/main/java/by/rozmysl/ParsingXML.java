package by.rozmysl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.*;

public class ParsingXML {
    private static final Logger logger = LoggerFactory.getLogger(ParsingXML.class);
    private static final List<String> oldFiles = new ArrayList<>();
    private static final List<String> newFiles = new ArrayList<>();
    private static long timeSpent;

    public static void renameFilesUsingXML() {
        long startTime = System.currentTimeMillis();
        try {
            logger.info("Config read");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.parse(".//working/config.xml");
            NodeList file = document.getElementsByTagName("file");

            logger.info("Renaming files");
            String suffix = document.getElementsByTagName("suffix").item(0).getTextContent();
            for (int i = 0; i < file.getLength(); i++) {
                String oldName = file.item(i).getTextContent();
                if (new File(oldName).isFile()) {
                    oldFiles.add(oldName);
                    String newName = oldName + suffix;
                    newFiles.add(newName);
                    logger.info(oldName + " -> " + newName);
                    //new File(oldName).renameTo(new File(newName));
                } else logger.warn("file " + oldName + " does not exist");

            }
        } catch (NullPointerException | ParserConfigurationException | SAXException | IOException ex) {
            logger.error(String.valueOf(ex));
        }
        timeSpent = System.currentTimeMillis() - startTime;
        createXMLReport();
    }

    private static void createXMLReport() {
        logger.info("Create XML Report");
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element root = document.createElement("report");
            document.appendChild(root);

            Element config = document.createElement("config");
            root.appendChild(config);
            Text text = document.createTextNode("config.xml");
            config.appendChild(text);

            Element time = document.createElement("completionTime");
            root.appendChild(time);
            Text text1 = document.createTextNode(String.valueOf(timeSpent));
            time.appendChild(text1);

            Element files = document.createElement("files");
            root.appendChild(files);
            for (int i = 0; i < oldFiles.size(); i++) {
                files.appendChild(getFile(document, oldFiles.get(i), newFiles.get(i)));
            }
            writeDocument(document);
        } catch (ParserConfigurationException ex) {
            logger.error(String.valueOf(ex));
        }

    }

    private static Node getFile(Document document, String oldName, String newName) {
        Element file = document.createElement("file");
        file.appendChild(getFileElements(document, "oldName", oldName));
        file.appendChild(getFileElements(document, "newName", newName));
        return file;
    }

    private static Node getFileElements(Document document, String name, String value) {
        Element node = document.createElement(name);
        node.appendChild(document.createTextNode(value));
        return node;
    }

    private static void writeDocument(Document document) {
        logger.info("Write and print XML Report");
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);

            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File(".//working/report.xml"));

            transformer.transform(source, console);
            transformer.transform(source, file);
        } catch (TransformerException ex) {
            logger.error(String.valueOf(ex));
        }
    }
}