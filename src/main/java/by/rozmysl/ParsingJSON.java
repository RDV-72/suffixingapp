package by.rozmysl;

import com.google.gson.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;

public class ParsingJSON {
    private static final Logger logger = LoggerFactory.getLogger(ParsingJSON.class);

    public static void renameFilesUsingJSON() throws IOException {
        logger.debug("Debug message");
        try {
            logger.info("Config read");
            String config = FileUtils.readFileToString(
                    new File(".//working/config.json"), "UTF-8");
            JsonParser parser = new JsonParser();
            String s = config.replace("\\", "\\\\");
            JsonElement rootNode = parser.parse(s);

            if (rootNode.isJsonObject()) {
                JsonObject details = rootNode.getAsJsonObject();
                JsonArray files = details.getAsJsonArray("files");
                String suffix = details.get("suffix").getAsString();
                logger.info("Renaming files");
                for (int i = 0; i < files.size(); i++) {
                    String oldName = files.get(i).getAsString();
                    if (new File(oldName).isFile()) {
                        String newName = oldName + suffix;
                        logger.info(oldName + " -> " + newName);
                        //new File(oldName).renameTo(new File(newName));
                    } else logger.warn("file " + oldName + " does not exist");
                }
            }
        } catch (NullPointerException | FileNotFoundException | JsonSyntaxException ex) {
            logger.error(String.valueOf(ex));
        }
    }
}