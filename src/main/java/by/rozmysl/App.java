package by.rozmysl;

import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    public static void main(String[] args) throws IOException {
        logger.info("Launch");
        ParsingJSON.renameFilesUsingJSON();
        ParsingXML.renameFilesUsingXML();
        logger.info("Work completed");
    }
}
